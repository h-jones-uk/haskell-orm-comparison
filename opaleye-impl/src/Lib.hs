{-# LANGUAGE Arrows #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE StandaloneDeriving #-}

module Lib where

-- Thoughts:
--
-- 1) Using product-profunctors for table definitions definitely ups
--    the amount of upfront cost needed.
-- 2) That said, this means that it's fairly easy to extend tables with
--    common columns; representing the table definitions at runtime
--    leads to... making abstraction *possible*. Maybe not easy.
-- 3) Through the same causes, it also makes it easy to define separate
--    views into the same table if necessary, and query in different
--    ways in different places.
-- 4) The choice between `optional` and `readOnly` for table fields is a
--    little subtle. For instance, making an autoincrementing ID field
--    `readOnly` is a bad idea, because then updates will attempt to
--    use DEFAULT as the update value every time. Not what you want.
--    Instead, use `optional` for those. I recommend using `optional` as
--    your default for fields you don't want to include and only use
--    `readOnly` if you know it's safe e.g. an update timestamp.
-- 5) Sadly, no way to explicitly run queries that only expect a single result.
--    Have to pattern match on the resulting list yourself.
-- 6) Opaleye uses Arrows for its main abstraction rather than monads, but
--    in practice that doesn't seem to matter much. They kind of just behave
--    like "funny-looking monads".
-- 7) For some reason it didn't quite hit me the first time how to run queries
--    with parameters (in the Arrow position), but duh: just pass it in using
--    the arrow operators.
-- 8) One unfortunate aspect is that Opaleye doesn't correctly handle sums and
--    counts, since in Postgres, the type of sum result might be different
--    than the type of the column being summed over. For instance, SUM(col)
--    where col is a BIGINT gives a NUMERIC result; Opaleye incorrectly
--    attempts to return a BIGINT, causing a runtime error. An explicit
--    `unsafeCoerceColumn` is required to fix this, which is annoying.
-- 9) Doing left joins gets kind of annoying, because of the lack of type inference
--    and having to break it out into a separate query.
-- 10) For the timestamps, one thing you could do is to intentionally create a
--     "wrapper" type for common fields, and have all the "extras" be a part of
--     that. That way, when querying, it's possible to ignore all the extra
--     metadata if you don't want it.

import Prelude hiding ( sum, min, max, null )

import Data.Text hiding ( groupBy, null )
import Data.Fixed
import Data.Int
import Data.Time
import Data.Function ( (&) )

import Data.Profunctor
import Data.Profunctor.Product
import Data.Profunctor.Product.TH ( makeAdaptorAndInstance )

import Control.Arrow

import Opaleye

mkUTCTime :: (Integer, Int, Int)
          -> (Int, Int, Pico)
          -> UTCTime
mkUTCTime (year, mon, day) (hour, min, sec) =
  UTCTime (fromGregorian year mon day)
          (timeOfDayToTime (TimeOfDay hour min sec))

timestamps :: TableFields
  (Maybe (F SqlTimestamptz), ())
  (F SqlTimestamptz, F SqlTimestamptz)
timestamps =
  p2 (optional "created_at", readOnly "updated_at")

-- |
-- Strip the timestamp information from a table to create a new
-- queryable table for convenience purposes.
withNoMeta :: Table (a, (Maybe (F SqlTimestamptz), ())) (b, c) -> Table a b
withNoMeta tbl = tbl
  & lmap (\t -> (t, (Nothing, ())))
  & rmap fst

type F field = Field field
type FNull field = FieldNullable field

newtype HandlerID = HandlerID Int32
  deriving (Show)
newtype HitmanID = HitmanID Int32
  deriving (Show)
newtype MarkID = MarkID Int32
  deriving (Show)

-- Showing that we can use our own custom IDs when defining our types.
instance QueryRunnerColumnDefault SqlInt4 HandlerID where
  defaultFromField = HandlerID <$> defaultFromField
instance QueryRunnerColumnDefault SqlInt4 HitmanID where
  defaultFromField = HitmanID <$> defaultFromField
instance QueryRunnerColumnDefault SqlInt4 MarkID where
  defaultFromField = MarkID <$> defaultFromField

--------------------
-- HANDLER
--------------------

data HandlerT a b = Handler
  { handlerID :: a
  , handlerCodename :: b
  }
type Handler = HandlerT HandlerID Text
type HandlerWrite = HandlerT (Maybe (F SqlInt4)) (F SqlText)
type HandlerF = HandlerT (F SqlInt4) (F SqlText)

deriving instance Show Handler

$(makeAdaptorAndInstance "pHandler" ''HandlerT)

handlerTable :: Table
  (HandlerWrite, (Maybe (F SqlTimestamptz), ()))
  (HandlerF, (F SqlTimestamptz, F SqlTimestamptz))
handlerTable = table "handlers" $
  (p2 ( pHandler Handler
          { handlerID = optional "id"
          , handlerCodename = required "codename"
          }
      , timestamps
      ))

-- |
-- Convenience table that ignores timestamps, for simpler queries.
handlersNoMeta :: Table HandlerWrite HandlerF
handlersNoMeta = withNoMeta handlerTable

--------------------
-- HITMAN
--------------------

data HitmanT a b c = Hitman
  { hitmanID :: a
  , hitmanCodename :: b
  , hitmanHandlerID :: c
  }
type Hitman = HitmanT HitmanID Text HandlerID
type HitmanWrite = HitmanT (Maybe (F SqlInt4)) (F SqlText) (F SqlInt4)
type HitmanF = HitmanT (F SqlInt4) (F SqlText) (F SqlInt4)

deriving instance Show Hitman

$(makeAdaptorAndInstance "pHitman" ''HitmanT)

hitmanTable :: Table
  (HitmanWrite, (Maybe (F SqlTimestamptz), ()))
  (HitmanF, (F SqlTimestamptz, F SqlTimestamptz))
hitmanTable = table "hitmen" $
  p2 ( pHitman $ Hitman
         { hitmanID = optional "id"
         , hitmanCodename = required "codename"
         , hitmanHandlerID = required "handler_id"
         }
     , timestamps
     )

-- |
-- Convenience table that ignores timestamps, for simpler queries.
hitmenNoMeta :: Table HitmanWrite HitmanF
hitmenNoMeta = withNoMeta hitmanTable

--------------------
-- MARK
--------------------

data MarkT a b c d e = Mark
  { markID :: a
  , markListBounty :: b
  , markFirstName :: c
  , markLastName :: d
  , markDescription :: e
  }
type Mark = MarkT MarkID Int64 Text Text (Maybe Text)
type MarkWrite = MarkT (Maybe (F SqlInt4)) (F SqlInt8) (F SqlText) (F SqlText) (FieldNullable SqlText)
type MarkF = MarkT (F SqlInt4) (F SqlInt8) (F SqlText) (F SqlText) (FieldNullable SqlText)

deriving instance Show Mark

$(makeAdaptorAndInstance "pMark" ''MarkT)

markTable :: Table
  (MarkWrite, (Maybe (F SqlTimestamptz), ()))
  (MarkF, (F SqlTimestamptz, F SqlTimestamptz))
markTable = table "marks" $
  p2 ( pMark Mark
         { markID = optional "id"
         , markListBounty = required "list_bounty"
         , markFirstName = required "first_name"
         , markLastName = required "last_name"
         , markDescription = tableField "description"
         }
     , timestamps
     )

-- |
-- Convenience table that ignores timestamps, for simpler queries.
marksNoMeta :: Table MarkWrite MarkF
marksNoMeta = withNoMeta markTable

--------------------
-- PURSUING MARK
--------------------

data PursuingMarkT a b = PursuingMark
  { pursuingMarkHitmanID :: a
  , pursuingMarkMarkID :: b
  }
type PursuingMark = PursuingMarkT HitmanID MarkID
type PursuingMarkF = PursuingMarkT (F SqlInt4) (F SqlInt4)

deriving instance Show PursuingMark

$(makeAdaptorAndInstance "pPursuingMark" ''PursuingMarkT)

pursuingMarkTable :: Table
  (PursuingMarkF, (Maybe (F SqlTimestamptz), ()))
  (PursuingMarkF, (F SqlTimestamptz, F SqlTimestamptz))
pursuingMarkTable = table "pursuing_marks" $
  p2 ( pPursuingMark PursuingMark
         { pursuingMarkHitmanID = required "hitman_id"
         , pursuingMarkMarkID = required "mark_id"
         }
     , timestamps
     )

-- |
-- Convenience table that ignores timestamps, for simpler queries.
pMarksNoMeta :: Table PursuingMarkF PursuingMarkF
pMarksNoMeta = withNoMeta pursuingMarkTable

--------------------
-- ERASED MARK
--------------------

data ErasedMarkT a b c = ErasedMark
  { erasedMarkHitmanID :: a
  , erasedMarkMarkID :: b
  , erasedMarkAwardedBounty :: c
  }
type ErasedMark = ErasedMarkT HitmanID MarkID Int64
type ErasedMarkF = ErasedMarkT (F SqlInt4) (F SqlInt4) (F SqlInt8)

deriving instance Show ErasedMark

$(makeAdaptorAndInstance "pErasedMark" ''ErasedMarkT)

erasedMarkTable :: Table
  (ErasedMarkF, (Maybe (F SqlTimestamptz), ()))
  (ErasedMarkF, (F SqlTimestamptz, F SqlTimestamptz))
erasedMarkTable = table "erased_marks" $
  p2 ( pErasedMark ErasedMark
         { erasedMarkHitmanID = required "hitman_id"
         , erasedMarkMarkID = required "mark_id"
         , erasedMarkAwardedBounty = required "awarded_bounty"
         }
     , timestamps
     )

-- |
-- Convenience table that ignores timestamps, for simpler queries.
eMarksNoMeta :: Table ErasedMarkF ErasedMarkF
eMarksNoMeta = withNoMeta erasedMarkTable

--------------------
-- QUERIES
--------------------

allHitmen :: Select HitmanF
allHitmen = selectTable hitmenNoMeta

activeMarks :: Select MarkF
activeMarks = proc () -> do
  (m, em) <- joined -< ()

  restrict -< isNull (erasedMarkMarkID em)

  returnA -< m

  where joined :: Select (MarkF, ErasedMarkT (FNull SqlInt4) (FNull SqlInt4) (FNull SqlInt8))
        joined = leftJoin (selectTable marksNoMeta)
                          (selectTable eMarksNoMeta)
                   (\(m, em) -> markID m .== erasedMarkMarkID em)

activelyPursuingMarks :: Select HitmanF
activelyPursuingMarks = proc () -> do
  h <- selectTable (rmap fst hitmanTable) -< ()
  pm <- selectTable (rmap fst pursuingMarkTable) -< ()
  active <- activeMarks -< ()

  restrict -< markID active .== pursuingMarkMarkID pm
  restrict -< hitmanID h .== pursuingMarkHitmanID pm

  returnA -< h

erasedSince :: SelectArr UTCTime MarkF
erasedSince = proc (since) -> do
  m <- selectTable marksNoMeta -< ()
  (ErasedMark { erasedMarkMarkID = eMarkID }, (createdAt, _))
    <- selectTable erasedMarkTable
    -< ()

  restrict -< createdAt .>= sqlUTCTime since
  restrict -< markID m .== eMarkID

  returnA -< m

erasedSinceBy :: SelectArr (UTCTime, HitmanID) MarkF
erasedSinceBy = proc (since, (HitmanID by)) -> do
  erased <- erasedSince -< since
  ErasedMark { erasedMarkHitmanID = hitmanID, erasedMarkMarkID = eMarkID }
    <- selectTable eMarksNoMeta
    -< ()

  restrict -< markID erased .== eMarkID .&& hitmanID .== sqlInt4 (fromIntegral by)

  returnA -< erased

-- |
-- Makes sure to return total bounty of zero even for hitmen who
-- don't have any erased marks.
totalBountiesAwarded :: Select (HitmanF, F SqlNumeric)
totalBountiesAwarded = rmap
  (\(h, (_, mTotal)) -> (h, fromNullable (sqlNumeric 0) mTotal))
  withDuds

  where totals :: Select (F SqlInt4, F SqlNumeric)
        totals =
          aggregate (p2 (groupBy, sum)) $
            arr (\ErasedMark { erasedMarkHitmanID = hitmanID
                             , erasedMarkAwardedBounty = awarded
                             }
                  -> (hitmanID, unsafeCoerceColumn awarded))
              <<< selectTable eMarksNoMeta

        withDuds :: Select (HitmanF, (FNull SqlInt4, (FNull SqlNumeric)))
        withDuds = leftJoin (selectTable hitmenNoMeta) totals $
                     \(h, (hID, _)) -> hitmanID h .== hID

totalBountyAwarded :: SelectArr HitmanID (F SqlNumeric)
totalBountyAwarded = proc (HitmanID to) -> do
  (h, total) <- totalBountiesAwarded -< ()

  restrict -< hitmanID h .== sqlInt4 (fromIntegral to)

  returnA -< total

latestHits :: Select (HitmanF, MarkF)
latestHits = proc () -> do
  (hID, created, mID) <- byDate -< ()
  (maxHID, maxCreated) <- maxDates -< ()
  h <- selectTable hitmenNoMeta -< ()
  m <- selectTable marksNoMeta -< ()

  restrict -< hID .== maxHID .&& created .== maxCreated
  restrict -< hID .== hitmanID h
  restrict -< mID .== markID m

  returnA -< (h, m)

  where byDate = aggregate (p3 (groupBy, groupBy, min)) $
                   arr (\( ErasedMark { erasedMarkHitmanID = hitmanID
                                      , erasedMarkMarkID = markID
                                      }
                         , (createdAt, _)
                         )
                         -> (hitmanID, createdAt, markID))
                     <<< selectTable erasedMarkTable
        maxDates = aggregate (p2 (groupBy, max)) $
                     arr (\( ErasedMark { erasedMarkHitmanID = hitmanID }
                           , (createdAt, _)
                           )
                           -> (hitmanID, createdAt))
                       <<< selectTable erasedMarkTable

latestHit ::  SelectArr HitmanID MarkF
latestHit = proc (HitmanID hitBy) -> do
  (h, m) <- latestHits -< ()
  restrict -< hitmanID h .== sqlInt4 (fromIntegral hitBy)
  returnA -< m

-- |
-- We exclude any marks that have already been erased.
singularPursuer :: Select (HitmanF, MarkF)
singularPursuer = proc () -> do
  h <- selectTable hitmenNoMeta -< ()
  m <- activeMarks -< ()
  (mID, numPursuers) <- pursuerCounts -< ()
  PursuingMark { pursuingMarkHitmanID = pHID, pursuingMarkMarkID = pMID }
    <- rmap fst (selectTable pursuingMarkTable)
    -< ()

  restrict -< numPursuers .== 1
  restrict -< mID .== pMID
  restrict -< mID .== markID m
  restrict -< pHID .== hitmanID h

  returnA -< (h, m)

  where pursuerCounts =
          aggregate (p2 (groupBy, countStar)) $
            arr (\PursuingMark { pursuingMarkHitmanID = hitmanID
                               , pursuingMarkMarkID = markID
                               }
                  -> (markID, hitmanID))
              <<< rmap fst (selectTable pursuingMarkTable)

marksOfOpportunity :: Select (HitmanF, MarkF)
marksOfOpportunity = proc () -> do
  h <- selectTable hitmenNoMeta -< ()
  m <- selectTable marksNoMeta -< ()
  ( ErasedMark { erasedMarkHitmanID = eHID, erasedMarkMarkID = eMID }
    , PursuingMark { pursuingMarkHitmanID = pHID }
    )
    <- joined
    -< ()

  restrict -< isNull pHID
  restrict -< hitmanID h .== eHID
  restrict -< markID m .== eMID

  returnA -< (h, m)

  where joined :: Select (ErasedMarkF, PursuingMarkT (FNull SqlInt4) (FNull SqlInt4))
        joined = leftJoin (rmap fst (selectTable erasedMarkTable))
                          (rmap fst (selectTable pursuingMarkTable)) $
          (\( ErasedMark { erasedMarkHitmanID = eHitmanID, erasedMarkMarkID = eMarkID }
            , PursuingMark { pursuingMarkHitmanID = pHitmanID, pursuingMarkMarkID = pMarkID }
            )
            -> eHitmanID .== pHitmanID .&& eMarkID .== pMarkID)

--------------------
-- INSERTS/UPDATES
--------------------

insertSeedHandlers :: Insert Int64
insertSeedHandlers = Insert
  { iTable = handlerTable
  , iRows =
    [ (Handler Nothing (sqlString "Olive"), (Nothing, ()))
    , (Handler Nothing (sqlString "Pallas"), (Nothing, ()))
    ]
  , iReturning = rCount
  , iOnConflict = Nothing
  }

insertSeedHitmen :: Insert Int64
insertSeedHitmen = Insert
  { iTable = hitmanTable
  , iRows =
    [ (Hitman Nothing (sqlString "Callaird") 1, (Nothing, ()))
    , (Hitman Nothing (sqlString "Bomois") 1, (Nothing, ()))
    , (Hitman Nothing (sqlString "Dune") 2, (Nothing, ()))
    ]
  , iReturning = rCount
  , iOnConflict = Nothing
  }

insertSeedMarks :: Insert Int64
insertSeedMarks = Insert
  { iTable = markTable
  , iRows =
    [ (Mark Nothing 25000 (sqlString "John") (sqlString "Tosti") null, (Nothing, ()))
    , (Mark Nothing 50000 (sqlString "Macie") (sqlString "Jordan") null, (Nothing, ()))
    , (Mark Nothing 33000 (sqlString "Sal") (sqlString "Aspot") null, (Nothing, ()))
    , (Mark Nothing 10000 (sqlString "Lars") (sqlString "Andersen") null, (Nothing, ()))
    ]
  , iReturning = rCount
  , iOnConflict = Nothing
  }

insertSeedPursuingMarks :: Insert Int64
insertSeedPursuingMarks = Insert
  { iTable = pursuingMarkTable
  , iRows =
    [ (PursuingMark 1 2, (Just $ sqlUTCTime (mkUTCTime (2018, 07, 01) (0, 0, 0)), ()))
    , (PursuingMark 2 2, (Just $ sqlUTCTime (mkUTCTime (2018, 07, 02) (0, 0, 0)), ()))
    , (PursuingMark 2 4, (Just $ sqlUTCTime (mkUTCTime (2019, 05, 05) (0, 0, 0)), ()))
    , (PursuingMark 3 3, (Just $ sqlUTCTime (mkUTCTime (2018, 05, 13) (0, 0, 0)), ()))
    , (PursuingMark 3 2, (Just $ sqlUTCTime (mkUTCTime (2019, 02, 15) (0, 0, 0)), ()))
    ]
  , iReturning = rCount
  , iOnConflict = Nothing
  }

insertSeedErasedMarks :: Insert Int64
insertSeedErasedMarks = Insert
  { iTable = erasedMarkTable
  , iRows =
    [ (ErasedMark 1 2 30000, (Just $ sqlUTCTime (mkUTCTime (2018, 09, 03) (0, 0, 0)), ()))
    , (ErasedMark 1 1 55000, (Just $ sqlUTCTime (mkUTCTime (2019, 02, 02) (0, 0, 0)), ()))
    , (ErasedMark 3 3 27000, (Just $ sqlUTCTime (mkUTCTime (2018, 06, 30) (0, 0, 0)), ()))
    ]
  , iReturning = rCount
  , iOnConflict = Nothing
  }

increaseListBounty :: Int64 -> MarkID -> Update Int64
increaseListBounty amt (MarkID id) = Update
  { uTable = markTable
  , uUpdateWith = \(m, (created, _)) ->
                    ( m { markID = Just (markID m)
                        , markListBounty = markListBounty m + toFields amt
                        }
                    , (Just created, ())
                    )
  , uWhere = \(m, _) -> markID m .== toFields id
  , uReturning = rCount
  }
